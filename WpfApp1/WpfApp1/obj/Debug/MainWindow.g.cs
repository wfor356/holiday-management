﻿#pragma checksum "..\..\MainWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "CFD7B50359C315721489DB1F4BC37BEEE7B009C44DF4BAFD05F43F5EE7CB9F45"
//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfApp1;


namespace WpfApp1 {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        /// <summary>
        /// holiday_management Name Field
        /// </summary>
        
        #line 2 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public WpfApp1.MainWindow holiday_management;
        
        #line default
        #line hidden
        
        /// <summary>
        /// maingrid Name Field
        /// </summary>
        
        #line 16 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Grid maingrid;
        
        #line default
        #line hidden
        
        /// <summary>
        /// uxDataGrid Name Field
        /// </summary>
        
        #line 18 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.DataGrid uxDataGrid;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn entry_year;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn working_years;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn holiday_division;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn edate;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn april_old;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn april_new;
        
        #line default
        #line hidden
        
        
        #line 178 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn april_use;
        
        #line default
        #line hidden
        
        
        #line 190 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn may_old;
        
        #line default
        #line hidden
        
        
        #line 202 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn may_new;
        
        #line default
        #line hidden
        
        
        #line 213 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn may_used;
        
        #line default
        #line hidden
        
        
        #line 225 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn june_old;
        
        #line default
        #line hidden
        
        
        #line 237 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn june_new;
        
        #line default
        #line hidden
        
        
        #line 248 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn june_used;
        
        #line default
        #line hidden
        
        
        #line 260 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn july_old;
        
        #line default
        #line hidden
        
        
        #line 272 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn july_new;
        
        #line default
        #line hidden
        
        
        #line 284 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn july_used;
        
        #line default
        #line hidden
        
        
        #line 298 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn august_old;
        
        #line default
        #line hidden
        
        
        #line 310 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn august_new;
        
        #line default
        #line hidden
        
        
        #line 321 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn august_used;
        
        #line default
        #line hidden
        
        
        #line 333 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn september_old;
        
        #line default
        #line hidden
        
        
        #line 345 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn september_new;
        
        #line default
        #line hidden
        
        
        #line 356 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn september_used;
        
        #line default
        #line hidden
        
        
        #line 368 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn october_old;
        
        #line default
        #line hidden
        
        
        #line 380 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn october_new;
        
        #line default
        #line hidden
        
        
        #line 391 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn october_used;
        
        #line default
        #line hidden
        
        
        #line 403 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn old_11;
        
        #line default
        #line hidden
        
        
        #line 415 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn new_11;
        
        #line default
        #line hidden
        
        
        #line 426 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn used_11;
        
        #line default
        #line hidden
        
        
        #line 438 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn december_old;
        
        #line default
        #line hidden
        
        
        #line 450 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn december_new;
        
        #line default
        #line hidden
        
        
        #line 461 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn december_used;
        
        #line default
        #line hidden
        
        
        #line 473 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn january_old;
        
        #line default
        #line hidden
        
        
        #line 485 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn january_new;
        
        #line default
        #line hidden
        
        
        #line 496 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn january_used;
        
        #line default
        #line hidden
        
        
        #line 508 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn february_old;
        
        #line default
        #line hidden
        
        
        #line 520 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn february_new;
        
        #line default
        #line hidden
        
        
        #line 531 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn february_used;
        
        #line default
        #line hidden
        
        
        #line 543 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn march_old;
        
        #line default
        #line hidden
        
        
        #line 555 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn march_new;
        
        #line default
        #line hidden
        
        
        #line 566 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn march_used;
        
        #line default
        #line hidden
        
        
        #line 581 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label yearlabel;
        
        #line default
        #line hidden
        
        
        #line 588 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OnOffButton;
        
        #line default
        #line hidden
        
        
        #line 591 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox years_box;
        
        #line default
        #line hidden
        
        
        #line 593 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button new_table_create;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfApp1;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.holiday_management = ((WpfApp1.MainWindow)(target));
            return;
            case 2:
            this.maingrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.uxDataGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 19 "..\..\MainWindow.xaml"
            this.uxDataGrid.CellEditEnding += new System.EventHandler<System.Windows.Controls.DataGridCellEditEndingEventArgs>(this.yearinput);
            
            #line default
            #line hidden
            
            #line 19 "..\..\MainWindow.xaml"
            this.uxDataGrid.KeyDown += new System.Windows.Input.KeyEventHandler(this.numberinput);
            
            #line default
            #line hidden
            
            #line 20 "..\..\MainWindow.xaml"
            this.uxDataGrid.CurrentCellChanged += new System.EventHandler<System.EventArgs>(this.currentchanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.entry_year = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 5:
            this.working_years = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 6:
            this.holiday_division = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 7:
            this.edate = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 8:
            this.april_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 9:
            this.april_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 10:
            this.april_use = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 11:
            this.may_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 12:
            this.may_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 13:
            this.may_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 14:
            this.june_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 15:
            this.june_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 16:
            this.june_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 17:
            this.july_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 18:
            this.july_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 19:
            this.july_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 20:
            this.august_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 21:
            this.august_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 22:
            this.august_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 23:
            this.september_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 24:
            this.september_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 25:
            this.september_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 26:
            this.october_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 27:
            this.october_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 28:
            this.october_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 29:
            this.old_11 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 30:
            this.new_11 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 31:
            this.used_11 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 32:
            this.december_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 33:
            this.december_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 34:
            this.december_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 35:
            this.january_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 36:
            this.january_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 37:
            this.january_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 38:
            this.february_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 39:
            this.february_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 40:
            this.february_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 41:
            this.march_old = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 42:
            this.march_new = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 43:
            this.march_used = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 44:
            this.yearlabel = ((System.Windows.Controls.Label)(target));
            return;
            case 45:
            
            #line 584 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.writerow);
            
            #line default
            #line hidden
            return;
            case 46:
            
            #line 585 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.deleterow);
            
            #line default
            #line hidden
            return;
            case 47:
            
            #line 586 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.AddBtn);
            
            #line default
            #line hidden
            return;
            case 48:
            
            #line 587 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.showmember);
            
            #line default
            #line hidden
            return;
            case 49:
            this.OnOffButton = ((System.Windows.Controls.Button)(target));
            
            #line 588 "..\..\MainWindow.xaml"
            this.OnOffButton.Click += new System.Windows.RoutedEventHandler(this.ButtonOnOff);
            
            #line default
            #line hidden
            return;
            case 50:
            this.years_box = ((System.Windows.Controls.TextBox)(target));
            return;
            case 51:
            
            #line 592 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.showdata);
            
            #line default
            #line hidden
            return;
            case 52:
            this.new_table_create = ((System.Windows.Controls.Button)(target));
            
            #line 593 "..\..\MainWindow.xaml"
            this.new_table_create.Click += new System.Windows.RoutedEventHandler(this.make_newyear);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

