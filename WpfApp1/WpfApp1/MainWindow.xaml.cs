﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MySql.Data.MySqlClient;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Windows.Forms;
using System.Globalization;
using System.Text.RegularExpressions;
using Cursors = System.Windows.Forms.Cursors;
using System.IO;
using AutoFixture;
using System.Windows.Controls.Primitives;
using DataGridCell = System.Windows.Controls.DataGridCell;
using DataGrid = System.Windows.Controls.DataGrid;


namespace WpfApp1

{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {

        static string connStr = "server=172.105.241.20; user id=user; password=garhijapan#02; database=holiday_management";


        static MySqlConnection conn = new MySqlConnection(connStr);
        //public static DataTable tbla;
        public static DataTable tblb = getholiday("t_member_informations", "t_holiday_remaining");
        

        //年度格納変数
        static int thisyear;

        //アプリ使用月を格納
        static int thismonth =DateTime.Now.Month;

        //各種SQL発行用変数
        static int sqlmonth;
        
        //使用月の編集可否を操作する変数
        int changecellmonth = DateTime.Now.Month;


        //データグリッド上での更新データをSQLにして保持するList
        static List<string> updatesql = new List<string>();


        //新規社員登録判定用変数
        public static bool newdata = false;

        //tblbの各月used指定用配列。使用月数＆残日計算で使用
        int[] usedmonth_array = { 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47 };

        //手入力切替フラグ。デフォルトはtrue。
        public static bool autocalc = true;

        //□選択セルが移動または確定したら直前のセルのSQLを生成するメソッド(currentcellchangedイベント)用変数
        String value = "";
        int cn = 0;
        int rn = 0;
        int cn2 = 0;

        //■■■有給残日数自動計算用の変数のため、保留
        //選択セルが11かチェックする変数
        int cn11check = 0;
        //beforeusedを格納したかのフラグ。0なら格納してない。1なら格納してる。
        int beforeusedcheck = 0;

        //選択セルが11の時、編集前の使用日数を格納する処理（676～）のための行番号格納変数
        int beforeused_row = -1;



        //使用日数編集前と編集後の値を格納する。
        int beforeused = 0;
        int nowused = 0;
        //編集後-編集前の値を格納する。
        int totalused = 0;

        //tblbの各use月のカラムNoを配列から取得
        int tblbcolumNo = 0;

        public MainWindow()
        {
            InitializeComponent();
            //sqlmonthの値を変更
            if(thismonth < 4)
            {
                sqlmonth = thismonth + 12;
            }
            else
            {
                sqlmonth = thismonth;
            }
            //Addnewmemberの更新フラグがTrueになっていたら表示
            Activated += (s, e) =>
            {
                if (newdata)
                {
                    newdata = false;
                    //処理中マウスカーソルを待機マークにする
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    
                    initialsort();
                    initialcalc();
                    dgupdate();
                    //処理中マウスカーソルを戻す
                    System.Windows.Forms.Cursor.Current = Cursors.Default;
                }
            };
            if (DateTime.Now.Month > 3)
            {
                thisyear = DateTime.Now.Year;
            }
            else
            {
                thisyear = DateTime.Now.Year - 1;
            }

            
            initialsort();
            initialcalc();

            dgupdate();

            //当月だけ有給使用日数を編集できるようにする
            if (changecellmonth > 3)
            {
                uxDataGrid.Columns[11 + 3 * (changecellmonth - 4)].IsReadOnly = false;
                Debug.WriteLine("changecellmonthは？" + changecellmonth);
                Debug.WriteLine("カラム名は？4～12月" + uxDataGrid.Columns[11 + 3 * (changecellmonth - 4)].Header);
            }
            else
            {
                Debug.WriteLine("カラム名は1～3月？" + uxDataGrid.Columns[35 + 3 * changecellmonth].Header);
                uxDataGrid.Columns[35 + 3 * changecellmonth].IsReadOnly = false;
            }




        }


        /// 色変えテスト！！！！！
        //private void Window_Loaded(object sender, EventArgs e)
        //{
        //    //データグリッドのセル数分繰り返します。
        //    var rowNum = uxDataGrid.Items.Count;
        //   for (int i = 0; i < rowNum; i++)
        //    {
        //        DataRow dr = tblb.Rows[i];
        //        DateTime addmonthdate = (DateTime)dr["additional_date"];
        //        int addmonth = addmonthdate.Month;
        //        Debug.WriteLine(i+"行addmonth"+addmonth);

        //        int addmonth_color;
        //        if (addmonth > 3)
        //        {
        //            addmonth_color = 10 + (addmonth - 4) * 3;
        //        }
        //        else
        //        {
        //            addmonth_color = 34 + addmonth * 3;
        //        }
        //        if(addmonth == 10)
        //        {

        //        }
        //    }

        //Debug.WriteLine("Window_ContentRendered");
        //// データグリッドのセル数分繰り返します。
        //var rowNum = uxDataGrid.Items.Count;
        //var columnNum = uxDataGrid.Columns.Count;

        //for (int i = 0; i < rowNum; i++)
        //{
        //    int test;


        //    //tblbからDataRowを1行ずつ作成し、付与月addmonthをintで取得する
        //    DataRow dr = tblb.Rows[i];
        //    DateTime addmonthdate = (DateTime)dr["additional_date"];
        //    int addmonth = addmonthdate.Month;
        //    Debug.WriteLine("行数："+ i +"月" + addmonth);
        //    if (addmonth == 7)
        //    {
        //        test = 10;
        //    }
        //    else
        //    {
        //        test = 1;
        //    }
        //    Debug.WriteLine("test" + test);
        //    //取得した付与月addmonthから色付けするカラムNoを算出する
        //    int changecolor_addmonth = 0;


        //    Debug.WriteLine(
        //        "行" + String.Format("{0,4}", i) +
        //        "\tdisplayindex" + String.Format("{0,10}", uxDataGrid.Columns[changecolor_addmonth].Header) +
        //        "\t付与月" + String.Format("{0,4}", addmonth) +
        //        "\t付与月カラム" + String.Format("{0,4}", changecolor_addmonth));

        //    uxDataGrid.UpdateLayout();
        //    uxDataGrid.ScrollIntoView(uxDataGrid.Items[i]);
        //    // データグリッドの行オブジェクトを取得します。
        //    DataGridRow row = (DataGridRow)uxDataGrid.ItemContainerGenerator.ContainerFromIndex(i); //rowはnullが出ないことを確認済み

        //    //Debug.Write(row.GetIndex());

        //    //rowから個別のセルを取得
        //    var cell = uxDataGrid.Columns[test].GetCellContent(row);
        //    //Debug.WriteLine("row" + cell.);
        //    if (cell == null)
        //    {
        //        // 対象のセルが表示されていない場合、セルオブジェクトが取得できないため
        //        // 対象のセルが表示されるようスクロールします。
        //        uxDataGrid.UpdateLayout();
        //        uxDataGrid.ScrollIntoView(uxDataGrid.Columns[test]);
        //        cell = uxDataGrid.Columns[test].GetCellContent(row);
        //    }

        //    if (cell == null)
        //    {
        //        Debug.WriteLine("cell=null");
        //        continue;
        //    }

        //    TextBlock textBlock = cell as System.Windows.Controls.TextBlock;
        //    var text = textBlock.Text;
        //    Debug.WriteLine(text); //ターゲットのセルはほぼ間違いなく取れてる模様
        //    textBlock.Background = Brushes.Green;




        //}


        //}

        // 内部メソッド(詳細は省略)

        //public DataGridCell GetDataGridCell(DataGrid dataGrid, int rowIndex, int columnIndex)
        //{

        //    if (dataGrid.Items == null || dataGrid.Items.IsEmpty)
        //    {
        //        Debug.WriteLine("null確認１");
        //        return null;
        //    }

        //    var row = GetDataGridRow(dataGrid, rowIndex);
        //    if (row == null)
        //    {
        //        Debug.WriteLine("null確認2");
        //        return null;
        //    }

        //    var presenter = GetVisualChild<DataGridCellsPresenter>(row);
        //    if (presenter == null)
        //    {
        //        Debug.WriteLine("null確認3");
        //        return null;
        //    }

        //    var generator = presenter.ItemContainerGenerator;
        //    var cell = generator.ContainerFromIndex(columnIndex) as DataGridCell;
        //    Debug.WriteLine("行"+rowIndex+"列"+columnIndex);
        //    if (cell == null)
        //    {
        //        uxDataGrid.UpdateLayout();
        //        var column = uxDataGrid.Columns[columnIndex];
        //        uxDataGrid.ScrollIntoView(row, column);
        //        cell = generator.ContainerFromIndex(columnIndex) as DataGridCell;
        //        Debug.WriteLine("nullの場合行" + rowIndex + "列" + columnIndex);
        //    }
        //    return cell;
        //}

        //public DataGridRow GetDataGridRow(DataGrid dataGrid, int index)
        //{
        //    if (dataGrid.Items == null || dataGrid.Items.IsEmpty)
        //    {
        //        return null;
        //    }

        //    var generator = uxDataGrid.ItemContainerGenerator;
        //    var row = generator.ContainerFromIndex(index) as DataGridRow;
        //    if (row == null)
        //    {
        //        uxDataGrid.UpdateLayout();
        //        var item = uxDataGrid.Items[index];
        //        uxDataGrid.ScrollIntoView(item);
        //        row = generator.ContainerFromIndex(index) as DataGridRow;
        //    }
        //    return row;
        //}

        //private T GetVisualChild<T>(Visual parent) where T : Visual
        //{
        //    T result = default(T);
        //    var count = VisualTreeHelper.GetChildrenCount(parent);
        //    for (int i = 0; i < count; ++i)
        //    {
        //        var child = VisualTreeHelper.GetChild(parent, i) as Visual;
        //        result = child as T;
        //        if (result != null)
        //        {
        //            break;
        //        }

        //        result = GetVisualChild<T>(child);
        //    }
        //    return result;
        //}
        ///// 色変えテストここまで！！！！！


        //入社日＆年度から付加月日、在籍年数、有効期限を算出するメソッド
        public void initialcalc()

        {
            tblb = getholiday("t_member_informations", "t_holiday_remaining");

            //自動更新SQL用変数
            String calcresult;

            //付与区分＆付与日数の配列。取り急ぎ実装⇒後でDB利用に変える（要修正）
            int[,] m_divisionArray = new int[,]
                {
                 {10,11,12,14,16,18,20} ,
                 {7,8,9,10,12,13,15},
                 {5,6,6,8,9,12,13},
                 {3,4,4,5,6,6,7},
                 {1,2,2,2,3,3,3}
                };

            foreach (DataRow r in tblb.Rows)
            {

                //入社年月日が今日以前で、入社年月日及び付与区分が設定されている時のみ自動計算を実行する。
                if (r["entry_date"] != null && r["division"] != null)
                {
                    Debug.WriteLine("ty" + thisyear);
                    //入社年月日を取得
                    DateTime entrydate = (DateTime)r["entry_date"];
                    //付加月日計算
                    int wm = getwm(entrydate, new DateTime(thisyear, 4, 1));
                    DateTime a = new DateTime(thisyear + 1, 3, 31);
                    DateTime b = new DateTime(thisyear, entrydate.Month, entrydate.Day).AddMonths(6);
                    DateTime Adddate = b;
                    if (b < a)
                    {
                        Adddate = b;
                    }
                    else
                    {
                        Adddate = new DateTime(thisyear, entrydate.Month, entrydate.Day).AddMonths(6);

                    }
                    TimeSpan interval = Adddate - entrydate;
                    //勤務年数の算出
                    String workingyear = Math.Floor(interval.TotalDays / 365) + "年" + Math.Abs(Adddate.Month - entrydate.Month) + "ヶ月";

                    //有効期限計算
                    DateTime effectivedate = Adddate.AddYears(2).AddDays(-1);

                    uint div = (uint)r["division"];

                    //付加日数計算(取り急ぎ実装)。後でDB使う方式に変える
                    Debug.WriteLine("td:" + interval.TotalDays);
                    int fordivision = (Int32)Math.Floor(interval.TotalDays / 365);

                    if (fordivision >= 7)
                    {
                        fordivision = 6;

                    }

                    Debug.WriteLine(div - 1 + ":" + fordivision);
                    int Addthisyear = m_divisionArray[div - 1, fordivision];

                    calcresult = "update t_member_informations set working_years" + "=\'" + workingyear + "\'" + "where member_id =" + r[0] + ";\r\n" +
                        "update t_member_informations set Additional_date" + "=\'" + Adddate + "\'" + "where member_id =" + r[0] + ";\r\n" +
                        "update t_member_informations set effective_date" + " =\'" + effectivedate + "\'" + "where member_id =" + r[0] + ";\r\n" +
                        "update t_member_informations set Addition" + " =\'" + Addthisyear + "\'" + "where member_id =" + r[0] + ";\r\n";
                    updatesql.Add(calcresult);


                }
                String allsql = "";
                foreach (String s in updatesql)
                {
                    allsql = allsql + s + "\r\n";
                }
                if (allsql != "")
                {
                    conn.Open();
                    Debug.WriteLine("月日数test:" + allsql);
                    MySqlCommand com = new MySqlCommand(allsql, conn);
                    com.ExecuteReader();
                    conn.Close();


                }
                updatesql.Clear();
                Debug.WriteLine(updatesql.Count);



            }

        }

        //一行のみ年度計算（未完成）
        public static void thisrowcalc(int rownum)
        {
            //選択行のDataRow を取得
            DataRow r = tblb.Rows[rownum];
            //自動更新SQL用変数
            String calcresult;
            //付与区分＆付与日数の配列。取り急ぎ実装⇒後でDB利用に変える（要修正）
            int[,] m_divisionArray = new int[,]
                {
                 {10,11,12,14,16,18,20} ,
                 {7,8,9,10,12,13,15},
                 {5,6,6,8,9,12,13},
                 {3,4,4,5,6,6,7},
                 {1,2,2,2,3,3,3}
                };
            //入社年月日及び付与区分が設定されている時のみ自動計算を実行する。
            if (r["entry_date"] != null && r["division"] != null)
            {
                //入社年月日を取得
                DateTime entrydate = (DateTime)r["entry_date"];
                //付加月日計算
                int wm = getwm(entrydate, new DateTime(thisyear, 4, 1));
                DateTime a = new DateTime(thisyear + 1, 3, 31);
                DateTime b = new DateTime(thisyear, entrydate.Month, entrydate.Day).AddMonths(6);
                DateTime Adddate = b;
                if (b < a)
                {
                    Adddate = b;
                }
                else
                {
                    Adddate = new DateTime(thisyear, entrydate.Month, entrydate.Day).AddMonths(6);
                    Debug.WriteLine(Adddate);
                }
                TimeSpan interval = Adddate - entrydate;
                //勤務年数の算出
                String workingyear = Math.Floor(interval.TotalDays / 365) + "年" + Math.Abs(Adddate.Month - entrydate.Month) + "ヶ月";

                //有効期限計算
                DateTime effectivedate = Adddate.AddYears(2).AddDays(-1);

                uint div = (uint)r["division"];
                //付加日数計算(取り急ぎ実装)。後でDB使う方式に変える
                int Addthisyear = m_divisionArray[div - 1, (Int32)Math.Floor(interval.TotalDays / 365)];

                calcresult = "update t_member_informations set working_years" + "=\'" + workingyear + "\'" + "where member_id =" + r[0] + ";\r\n" +
                    "update t_member_informations set Additional_date" + "=\'" + Adddate + "\'" + "where member_id =" + r[0] + ";\r\n" +
                    "update t_member_informations set effective_date" + " =\'" + effectivedate + "\'" + "where member_id =" + r[0] + " and years=" + thisyear + ";\r\n" +
                    "update t_member_informations set Addition" + " =\'" + Addthisyear + "\'" + "where member_id =" + r[0] + " and years=" + thisyear + ";\r\n";
                updatesql.Add(calcresult);
            }
            String allsql = "";
            foreach (String s in updatesql)
            {
                allsql = allsql + s + "\r\n";
            }
            if (allsql != "")
            {
                conn.Open();
                Debug.WriteLine("月日数test:" + allsql);
                MySqlCommand com = new MySqlCommand(allsql, conn);
                com.ExecuteReader();
                conn.Close();

            }

            updatesql.Clear();
            Debug.WriteLine(updatesql.Count);
        }



        //データグリッドの当月⇒4月のソートメソッド
        private void initialsort()
        {
            //ソート
            int sortmonth = thismonth;
            int j = 0;
            if (sortmonth < 4)
            {
                for (int i = sortmonth + 12; i > 12; i--)
                {
                    uxDataGrid.Columns[9 + (i - 4) * 3].DisplayIndex = 9 + j;
                    uxDataGrid.Columns[9 + (i - 4) * 3].Visibility = Visibility.Visible;
                    j++;
                    uxDataGrid.Columns[10 + (i - 4) * 3].DisplayIndex = 9 + j;
                    uxDataGrid.Columns[10 + (i - 4) * 3].Visibility = Visibility.Visible;
                    j++;
                    uxDataGrid.Columns[11 + (i - 4) * 3].DisplayIndex = 9 + j;
                    uxDataGrid.Columns[11 + (i - 4) * 3].Visibility = Visibility.Visible;
                    j++;
                }
                sortmonth = 12;
            }
            for (int i = sortmonth; i > 3; i--)
            {
                uxDataGrid.Columns[9 + (i - 4) * 3].DisplayIndex = 9 + j;
                uxDataGrid.Columns[9 + (i - 4) * 3].Visibility = Visibility.Visible;
                j++;
                uxDataGrid.Columns[10 + (i - 4) * 3].DisplayIndex = 9 + j;
                uxDataGrid.Columns[10 + (i - 4) * 3].Visibility = Visibility.Visible;
                j++;
                uxDataGrid.Columns[11 + (i - 4) * 3].DisplayIndex = 9 + j;
                uxDataGrid.Columns[11 + (i - 4) * 3].Visibility = Visibility.Visible;
                j++;
            }


        }

        

        public void dgupdate()
        {
            tblb = getholiday("t_member_informations", "t_holiday_remaining");
            yearlabel.Content = +thisyear + "年度管理表";
            //データグリッドに表示させる

            uxDataGrid.DataContext = tblb;
        }




        //uxDataGrid表示用の社員・有給テーブルデータの取得＆結合
        public static DataTable getholiday(string tableName1, String tableName2)
        {
            DataTable tbl = new DataTable();
            conn.Open();
            using (var command = conn.CreateCommand())
            {
                command.CommandText = $"SELECT * FROM {tableName1} INNER JOIN {tableName2} on {tableName1}.member_id = " +
                    $"{tableName2}.member_id WHERE {tableName2}.years = {thisyear} ORDER BY {tableName1}.member_of,{tableName1}.member_id ASC";
                using (var reader = command.ExecuteReader())
                {
                    tbl.Load(reader);
                }
            }
            conn.Close();
            return tbl;

        }



        //勤務月数の算出メソッド
        public static int getwm(DateTime dt1, DateTime dt2)
        {
            // 経過月数を求める(満月数を考慮しない単純計算)
            var wm = (dt2.Year - dt1.Year) * 12 + (dt2.Month - dt1.Month);

            if (dt1.Day <= dt2.Day)
                // baseDayの日部分がdayの日部分以上の場合は、その月を満了しているとみなす
                // (例:1月30日→3月30日以降の場合は満(3-1)ヶ月)
                return wm;
            else if (dt2.Day == DateTime.DaysInMonth(dt2.Year, dt2.Month) && dt2.Day <= dt1.Day)
                // baseDayの日部分がdayの表す月の末日以降の場合は、その月を満了しているとみなす
                // (例:1月30日→2月28日(平年2月末日)/2月29日(閏年2月末日)以降の場合は満(2-1)ヶ月)
                return wm;
            else
                // それ以外の場合は、その月を満了していないとみなす
                // (例:1月30日→3月29日以前の場合は(3-1)ヶ月未満、よって満(3-1-1)ヶ月)
                return wm;
        }




        /// 編集したセルの保存

        private void writerow(object sender, RoutedEventArgs e)
        {
            String allsql = "";
            foreach (String s in updatesql)
            {
                allsql = allsql + s + "\r\n";
            }
            if (allsql != "")
            {
                conn.Open();
                Debug.WriteLine("test:" + allsql);
                MySqlCommand com = new MySqlCommand(allsql, conn);
                com.ExecuteReader();
                conn.Close();
                initialcalc();
                System.Windows.MessageBox.Show("編集内容を保存しました。");


            }
            updatesql.Clear();
            dgupdate();
            Debug.WriteLine(updatesql.Count);

        }

        private void writerowsoon()
        {
            String allsql = "";
            foreach (String s in updatesql)
            {
                allsql = allsql + s + "\r\n";
            }
            if (allsql != "")
            {
                conn.Open();
                Debug.WriteLine("test:" + allsql);
                MySqlCommand com = new MySqlCommand(allsql, conn);
                com.ExecuteReader();
                conn.Close();

            }
            updatesql.Clear();
            dgupdate();
            Debug.WriteLine(updatesql.Count);
        }

        //年度別データ閲覧
        public void showdata(object sender, RoutedEventArgs e)
        {
            //処理中マウスカーソルを待機マークにする
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

            String selectyear = years_box.Text;
            try
            {
                thisyear = int.Parse(selectyear);
            }
            catch
            {
                System.Windows.MessageBox.Show("年度読み込みでエラーが発生しました");
                return;
            }




            Debug.WriteLine("年度" + selectyear);
            DataTable tbl1 = new DataTable();
            DataTable tbl2 = new DataTable();
            conn.Open();
            using (var command = conn.CreateCommand())
            {
                command.CommandText = $"SELECT * FROM t_member_informations INNER JOIN t_holiday_remaining on t_member_informations.member_id = " +
                $"t_holiday_remaining.member_id WHERE t_holiday_remaining.years =\'" + thisyear + "\'ORDER BY t_member_informations.member_id ASC";
                using (var reader = command.ExecuteReader())
                {
                    tbl1.Load(reader);
                }
            }
            conn.Close();
            //データが無ければreturn
            if (tbl1.Rows.Count == 0)
            {
                System.Windows.MessageBox.Show("その年度のデータがありません。");
                years_box.Clear();
                return;
            }
            initialcalc();
            conn.Open();
            using (var command = conn.CreateCommand())
            {
                command.CommandText = $"SELECT * FROM t_member_informations INNER JOIN t_holiday_remaining on t_member_informations.member_id = " +
                $"t_holiday_remaining.member_id WHERE t_holiday_remaining.years =\'" + thisyear + "\'ORDER BY t_member_informations.member_id ASC";
                using (var reader = command.ExecuteReader())
                {
                    tbl2.Load(reader);
                }
            }
            conn.Close();

            //データグリッドに表示させる

            initialsort();
            uxDataGrid.DataContext = tbl2;

            //左上の年度表示を更新
            yearlabel.Content = +thisyear + "年度管理表";

            //年度入力欄のクリア
            years_box.Clear();

            //thisyearを元に戻す
            if (DateTime.Now.Month > 3)
            {
                thisyear = DateTime.Now.Year;
            }
            else
            {
                thisyear = DateTime.Now.Year - 1;
            }


            //マウスカーソルを元に戻す
            System.Windows.Forms.Cursor.Current = Cursors.Default;

        }



        //社員追加ウィンドウを開く
        private void AddBtn(object sender, RoutedEventArgs e)
        {
            var win = new Addnewmember(thisyear);
            win.Show();
        }

        //選択行削除ボタンメソッド
        private void deleterow(object sender, RoutedEventArgs e)
        {
            String RowCnt;
            try
            {
                RowCnt = ((TextBlock)uxDataGrid.Columns[1].GetCellContent(uxDataGrid.SelectedItem)).Text;
            }
            catch
            {
                return;
            }
            DialogResult askdelete =
            System.Windows.Forms.MessageBox.Show(RowCnt + "さんのレコードを削除しますか?",
                                                        "削除の確認",
                                                         MessageBoxButtons.OKCancel,
                                                        MessageBoxIcon.Exclamation,
                                                        MessageBoxDefaultButton.Button2);
            if (askdelete == System.Windows.Forms.DialogResult.OK)
            {
                string tsc1 = "delete from t_member_informations where names = \'" + RowCnt + "\';\r\ndelete from t_holiday_remaining where names = \'" + RowCnt + "\'";

                conn.Open();
                MySqlCommand com1 = new MySqlCommand(tsc1, conn);
                com1.ExecuteReader();
                conn.Close();

                dgupdate();

            }

            else if (askdelete == System.Windows.Forms.DialogResult.Cancel)
            {

            }


        }



        //半角数字セル入力規則メソッド
        private void numberinput(object sender, System.Windows.Input.KeyEventArgs e)
        {
            
            int columnIndex;
        
            try
            {
                columnIndex = uxDataGrid.CurrentCell.Column.DisplayIndex;
            }
            catch (Exception)
            {
                columnIndex = 0;
                return;
            }

            //「社員No.」「付与区分」「当年付加日数」「各月の有給残日数または使用数」列の時
            if (columnIndex == 0 || columnIndex == 6 || columnIndex == 7 || columnIndex > 8)
            {
                
                //半角数字のみ入力可とする。
                bool rgx = Regex.IsMatch(e.Key.ToString(), "[0-9]+");
                if (!rgx)
                {
                    System.Windows.MessageBox.Show("半角数字を入力してください。");
                    e.Handled = true;
                }
            }



        }

        //Tab押下時のSQL発行メソッドtabchangedを呼び出すメソッド
        //private void tabinput(object sender, System.Windows.Input.KeyEventArgs e)
        //{
        //    if (e.Key == Key.Tab)
        //    {
        //            Debug.WriteLine("tab");
        //            uxDataGrid.CommitEdit();
        //            tabchanged();                    
        //    }
        //}


        //年度セル入力規則メソッド＆SQL生成メソッド（セルの編集が確定したときに実行される）
        private void yearinput(object sender, DataGridCellEditEndingEventArgs e)
        {
            //入力値を格納する変数
            String value;
            //編集したカラムNoを格納する変数
            int columnIndex;

            //ダブルクリック等で列番号取得に失敗したらreturn
            try
            {
                columnIndex = uxDataGrid.CurrentCell.Column.DisplayIndex;
            }
            catch (Exception g)
            {
                Debug.WriteLine(g);
                return;
            }

            if (columnIndex == 2)
            {
                DataGridCellInfo cell = uxDataGrid.SelectedCells[2];
                try
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch (Exception)
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }

                bool rgx2 = Regex.IsMatch(value, @"^[0-9]{4}/(0[1-9]|1[0-2])/(0[1-9]|[12][0-9]|3[01])$");
                if (!rgx2)
                {
                    System.Windows.MessageBox.Show("日付形式（例：2021/01/01）で入力してください。");

                }

            }

            else if (columnIndex == 5)
            {
                DataGridCellInfo cell = uxDataGrid.SelectedCells[5];
                try
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch (Exception)
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                bool rgx2 = Regex.IsMatch(value, @"^[0-9]{4}/(0[1-9]|1[0-2])/(0[1-9]|[12][0-9]|3[01])$");
                if (!rgx2)
                {
                    System.Windows.MessageBox.Show("日付形式（例：2021/01/01）で入力してください。");

                }


            }

            else if (columnIndex == 8)
            {
                DataGridCellInfo cell = uxDataGrid.SelectedCells[8];
                try
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch (Exception)
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                bool rgx2 = Regex.IsMatch(value, @"^[0-9]{4}/(0[1-9]|1[0-2])/(0[1-9]|[12][0-9]|3[01])$");
                if (!rgx2)
                {
                    System.Windows.MessageBox.Show("日付形式（例：2021/01/01）で入力してください。");

                }

            }
            //有給残日・使用数カラムが空白なら０に置換
            else if(columnIndex > 8)
            {
                DataGridCellInfo cell = uxDataGrid.SelectedCells[columnIndex];
                try
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                    if(value == "")
                    {
                        value = "0";
                        ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text = "0";
                    }
                }
                catch (Exception)
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                    if (value == "")
                    {
                        value = "0";
                        ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text="0";
                    }
                }
                
            }



        }

        


        private void currentchanged(object sender, EventArgs e)
        {
            Debug.WriteLine("currentchanged実行.cn=" + cn + ".rn=" + rn);


            //■■■有給残日減産処理は動作不具合あり、保留。
            //有給使用日数計算のために、セル確定前の処理が必要
            //列noが11かチェックする変数に列Noを格納
            try
            {
                cn11check = uxDataGrid.CurrentCell.Column.DisplayIndex;

            }
            catch
            {
                cn11check = 0;
            }


            Debug.WriteLine("11checkは？" + cn11check);

            //列番号11の当月使用日数欄でセル編集が行われたなら編集前後の使用日数の差から残日数の減算処理を行う（UPDATEはこちらの処理である事に注意！）
            if (beforeusedcheck == 1 && autocalc)
            {

                Debug.WriteLine("busedは？" + beforeused_row);
                //tblbの各use月のカラムNoを配列から取得
                int tblbcolumNo = 0;
                if (thismonth <= 3)
                {
                    tblbcolumNo = usedmonth_array[thismonth + 8];

                }
                else if (4 <= thismonth)
                {
                    tblbcolumNo = usedmonth_array[thismonth - 4];
                }
                Debug.WriteLine("tblbcolumNo:" + tblbcolumNo);

                //前にセルフォーカスが当たっていたセルの行でDataRowを生成する
                DataRow dr = tblb.Rows[beforeused_row];

                //現在の入力値。選択行のDataRowから当月の使用日数データを取得する。TinyintなのでConvert.ToByteでintにする
                nowused = Convert.ToByte(dr[tblbcolumNo]);

                //現在の入力値をStringにしてAddsqlに渡す。
                string value = nowused.ToString();


                //編集確定後の使用日数。編集前からこの値を引いた値処理を行う。使用日数が減った場合にも対応できるはず。

                Debug.WriteLine("beforeusedは?その２" + beforeused);
                Debug.WriteLine("nowusedは?" + nowused);
                //編集前のusedから編集後の値を引く
                totalused = nowused - beforeused;
                Debug.WriteLine("nowusedとbeforeuse確認。nowused:" + nowused + "beforeused:" + beforeused + "totalused:" + totalused);
                Addsql(value, tblbcolumNo - 4, beforeused_row);

                //すぐUPDATE実行
                Debug.WriteLine("newuseintは" + newuseint + "totalusedは" + totalused);
                if (newuseint >= 0 && totalused != 0)
                {

                    writerowsoon();
                    Debug.WriteLine("check!");
                }


                //beforeused_rowをリセットする
                beforeused_row = -1;
                //beforeusedcheckをリセットする
                beforeusedcheck = 0;

                //処理を終了
                return;

            }

            //選択列が11番＝当月使用日数欄＆現在の選択行Noが取得されているなら下記の処理を実行。まず使用日数が更新されたか確認のため、現在の使用日数を取得する
            //こっちの処理で「編集前の使用日数」を取得する。
            if (cn11check == 11 && beforeused_row <= 0 && beforeusedcheck == 0 && autocalc)
            {
                beforeused_row = uxDataGrid.Items.IndexOf(uxDataGrid.CurrentItem);//列が11なら行noを取得する
                Debug.WriteLine("Add:" + beforeused_row);
                Debug.WriteLine("列11時の処理確認：beforeused_rowは？" + beforeused_row);

                if (thismonth <= 3)
                {
                    tblbcolumNo = usedmonth_array[thismonth + 8];

                }
                else
                {
                    tblbcolumNo = usedmonth_array[thismonth - 4];
                }
                Debug.WriteLine("tbcmは？：" + tblbcolumNo);
                //選択セルの行でDataRowを生成する
                DataRow dr = tblb.Rows[beforeused_row];
                //選択行のDataRowから当月の使用日数データを取得する。TinyintなのでConvert.ToByteでintにする
                try
                {
                    beforeused = Convert.ToByte(dr[tblbcolumNo]);
                }
                catch
                {
                    beforeused = 0;
                }
                //編集確定前の使用日数。こっから編集後の使用日数を引いた値で処理を行う。使用日数が減った場合にも対応できるはず。
                Debug.WriteLine("beforeusedは?その１" + beforeused);
                //格納チェックを1に
                beforeusedcheck = 1;

            }

            //セルが移動したら入力確定させる。
            uxDataGrid.CommitEdit();
                        
            if (cn != 0 && cn < 9 && rn != -1)
            {

                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }

                //年度セルの誤入力対応
                if (cn == 2 || cn == 5 || cn == 8)
                {
                    try
                    {
                        DateTime dt = DateTime.Parse(value);
                        Addsql(value, cn, rn);
                    }
                    catch
                    {
                        return;
                    }
                }
                else
                {
                    Addsql(value, cn, rn);
                }

            }
            //下は取り急ぎ実装。今後修正する。(要修正)
            else if (cn > 8 && cn < 12)
            {
                cn2 = cn + (sqlmonth - 4) * 3;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                Debug.WriteLine(cell);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }


                Addsql(value, cn, rn);
            }

            else if (cn > 8 && cn < 15)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 6;
                DataGridCellInfo cell;
                try
                {
                    cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                }
                catch
                {
                    Debug.WriteLine("return確認");
                    return;
                }

                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }

                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);

            }
            else if (cn > 8 && cn < 18)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 12;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 21)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 18;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 24)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 24;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 27)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 30;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 30)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 36;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {

                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 33)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 42;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 36)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 48;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 39)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 54;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 42)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 60;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 45)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 66;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }


            //カーソルが当たった行Noを取得
            try
            {
                rn = uxDataGrid.Items.IndexOf(uxDataGrid.CurrentItem);//行no
                cn = uxDataGrid.CurrentCell.Column.DisplayIndex;    //列no
            }
            catch (Exception)
            {
                //保存ボタンを押すと何故かカラムNo－１（null）をとるので、0にリセットする
                cn = 0;
                return;
            }

            Debug.WriteLine("行：" + rn + "列：" + cn);


        }//curentchangeここまで

        private void tabchanged()
        {
            Debug.WriteLine("tabchanged実行.cn=" + cn + ".rn=" + rn);


            //■■■有給残日減産処理は動作不具合あり、保留。
            //有給使用日数計算のために、セル確定前の処理が必要
            //列noが11かチェックする変数に列Noを格納
            try
            {
                cn11check = uxDataGrid.CurrentCell.Column.DisplayIndex;

            }
            catch
            {
                cn11check = 0;
            }


            Debug.WriteLine("11checkは？" + cn11check);

            //列番号11の当月使用日数欄でセル編集が行われたなら編集前後の使用日数の差から残日数の減算処理を行う（UPDATEはこちらの処理である事に注意！）
            if (beforeusedcheck == 1 && autocalc)
            {

                Debug.WriteLine("busedは？" + beforeused_row);
                //tblbの各use月のカラムNoを配列から取得
                int tblbcolumNo = 0;
                if ((thismonth <= 3))
                {
                    tblbcolumNo = usedmonth_array[thismonth + 8];

                }
                else if (4 <= thismonth)
                {
                    tblbcolumNo = usedmonth_array[thismonth - 4];
                }
                Debug.WriteLine("tblbcolumNo:" + tblbcolumNo);

                //前にセルフォーカスが当たっていたセルの行でDataRowを生成する
                DataRow dr = tblb.Rows[beforeused_row];

                //現在の入力値。選択行のDataRowから当月の使用日数データを取得する。TinyintなのでConvert.ToByteでintにする
                nowused = Convert.ToByte(dr[tblbcolumNo]);

                //現在の入力値をStringにしてAddsqlに渡す。
                string value = nowused.ToString();


                //編集確定後の使用日数。編集前からこの値を引いた値処理を行う。使用日数が減った場合にも対応。

                Debug.WriteLine("beforeusedは?その２" + beforeused);
                Debug.WriteLine("nowusedは?" + nowused);
                //編集前のusedから編集後の値を引く
                totalused = nowused - beforeused;
                Debug.WriteLine("nowusedとbeforeuse確認。nowused:" + nowused + "beforeused:" + beforeused + "totalused:" + totalused);
                Addsql(value, tblbcolumNo - 4, beforeused_row);

                //すぐUPDATE実行
                Debug.WriteLine("newuseintは" + newuseint + "totalusedは" + totalused);
                if (newuseint >= 0 && totalused != 0)
                {

                    writerowsoon();
                    Debug.WriteLine("check!");
                }


                //beforeused_rowをリセットする
                beforeused_row = -1;
                //beforeusedcheckをリセットする
                beforeusedcheck = 0;

                //処理を終了
                return;

            }

            //選択列が11番＝当月使用日数欄＆現在の選択行Noが取得されているなら下記の処理を実行。まず使用日数が更新されたか確認のため、現在の使用日数を取得する
            //こっちの処理で「編集前の使用日数」を取得する。
            if (cn11check == 11 && beforeused_row <= 0 && beforeusedcheck == 0 && autocalc)
            {
                beforeused_row = uxDataGrid.Items.IndexOf(uxDataGrid.CurrentItem);//列が11なら行noを取得する
                Debug.WriteLine("Add:" + beforeused_row);
                Debug.WriteLine("列11時の処理確認：beforeused_rowは？" + beforeused_row);

                if (thismonth <= 3)
                {
                    tblbcolumNo = usedmonth_array[thismonth + 8];

                }
                else
                {
                    tblbcolumNo = usedmonth_array[thismonth - 4];
                }
                Debug.WriteLine("tbcmは？：" + tblbcolumNo);
                //選択セルの行でDataRowを生成する
                DataRow dr = tblb.Rows[beforeused_row];
                
                //編集確定前の使用日数。こっから編集後の使用日数を引いた値で処理を行う。使用日数が減った場合にも対応できるはず。
                Debug.WriteLine("beforeusedは?その１" + beforeused);
                //格納チェックを1に
                beforeusedcheck = 1;

            }

            //セルが移動したら入力確定させる。
            uxDataGrid.CommitEdit();

            if (cn != 0 && cn < 9 && rn != -1)
            {

                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }

                //年度セルの誤入力対応
                if (cn == 2 || cn == 5 || cn == 8)
                {
                    try
                    {
                        DateTime dt = DateTime.Parse(value);
                        Addsql(value, cn, rn);
                    }
                    catch
                    {
                        return;
                    }
                }
                else
                {
                    Addsql(value, cn, rn);
                }

            }
            //下は取り急ぎ実装。今後修正する。(要修正)
            else if (cn > 8 && cn < 12)
            {
                cn2 = cn + (sqlmonth - 4) * 3;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }


                Addsql(value, cn, rn);
            }

            else if (cn > 8 && cn < 15)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 6;
                DataGridCellInfo cell;
                try
                {
                    cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                }
                catch
                {
                    Debug.WriteLine("return確認");
                    return;
                }

                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }

                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);

            }
            else if (cn > 8 && cn < 18)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 12;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 21)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 18;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 24)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 24;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 27)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 30;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 30)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 36;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {

                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 33)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 42;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 36)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 48;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 39)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 54;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 42)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 60;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }
            else if (cn > 8 && cn < 45)
            {
                cn2 = cn + (sqlmonth - 4) * 3 - 66;
                DataGridCellInfo cell = new DataGridCellInfo(uxDataGrid.Items[rn], uxDataGrid.Columns[cn2]);
                try
                {
                    value = ((System.Windows.Controls.TextBlock)cell.Column.GetCellContent(cell.Item)).Text;
                }
                catch
                {
                    value = ((System.Windows.Controls.TextBox)cell.Column.GetCellContent(cell.Item)).Text;
                }
                //valueがnullなら0を格納
                if (value == "")
                {
                    value = "0";
                }
                Addsql(value, cn, rn);
            }


            //カーソルが当たった行Noを取得
            try
            {
                rn = uxDataGrid.Items.IndexOf(uxDataGrid.CurrentItem);//行no
                cn = uxDataGrid.CurrentCell.Column.DisplayIndex;    //列no
            }
            catch (Exception)
            {
                //保存ボタンを押すと何故かカラムNo－１（null）をとるので、0にリセットする
                cn = 0;
                return;
            }

            Debug.WriteLine("行：" + rn + "列：" + cn);


        }//curentchange　Tabキー用SQL発行メソッド

        //■セル更新時のUPDATE文生成メソッド(List updatesqlに格納)
        //旧残日と使用日数をint格納する変数（旧残日マイナス使用日数で計算する）
        //旧残日数の数値変数
        int oldint;
        //新残日数の数値変数
        int newint;

        //旧残日から使用日数を引いた数値変数
        int olduseint;

        //新残日からolduseintを引いた数値変数
        int newuseint = 0;

        private void Addsql(String value, int cnum, int rownum)
        {
                        
            Debug.WriteLine("sqlmonth"+sqlmonth+"thismonth"+thismonth);
            String thissql;
            DataRow row = tblb.Rows[0];
            //データグリッド上の選択行の行番号を取得
            int rn = uxDataGrid.Items.IndexOf(uxDataGrid.CurrentItem);
            String cn;
            List<string> clist = new List<string>();
            //データテーブルのカラム名を取得(アプリの最初に一度だけでOKか？試したが上手く行かず)要修正

            foreach (DataColumn column in tblb.Columns)
            {
                //DATATABLEでカラム名が”〇〇１”に変わったものを元に戻す
                cn = column.ColumnName;
                if (cn == "years" || cn == "member_id1" || cn == "names1" || cn == "years1")
                {

                }

                else
                {
                    clist.Add(cn);

                }


            }
            for(int i = 0;i < clist.Count; i++)
            {
                Debug.WriteLine(i+"番目"+clist[i]);
            }

            //ボタン等選択して行数-1になったらreturn
            if (rn == -1)
            {
                return;
            }
            else
            {
                row = tblb.Rows[rownum];

            }

            if (cnum == 1)
            {
                thissql = "update t_member_informations set " + clist[cnum] + "=\'" + value + "\'" + "where member_id =" + row[0] +
                    ";\n\r update t_holiday_remaining set " + clist[cnum] + "=\'" + value + "\'" + "where member_id =" + row[0] + " and years=" + thisyear + ";";
            }
            else if (cnum < 9)
            {
                thissql = "update t_member_informations set " + clist[cnum] + "=\'" + value + "\'" + "where member_id =" + row[0] + ";";
            }
            else
            {
                //当月⇒4月にソートするので、SQL用の列指定（何月かの指定）を行うwriterow
                //下は取り急ぎ実装。今後修正する。(要修正)


                ////■■■使用日数が増えたら残日数から減らす処理。cnum11は当月（アプリ立ち上げてる時の月）の使用日数のセルになる。
                if (cnum == tblbcolumNo - 4)
                {
                    //cnumとカラム名リストがちょいずれてるので調整
                    cnum = cnum + 1;

                    Debug.WriteLine("列番号11の時残日数減らす＆sql作る処理動作確認：totalusedは？" + totalused);

                    //編集後の使用日数-編集前の使用日数の変数totalusedが0なら使用日数の編集は行われていないとしてreturnする。    
                    if (totalused == 0)
                    {
                        return;
                    }



                    DataRow dr = tblb.Rows[rownum];
                    try
                    {
                        oldint = Convert.ToByte(dr[tblbcolumNo - 2]);
                    }
                    catch
                    {
                        oldint = 0;
                    }
                    Debug.WriteLine("旧残日数：" + oldint);

                    try
                    {
                        newint = Convert.ToByte(dr[tblbcolumNo - 1]);
                    }
                    catch
                    {
                        newint = 0;
                    }
                    Debug.WriteLine("新残日" + newint);

                    //旧残日から編集後の使用日数-編集前の使用日数totalusedを引く
                    olduseint = oldint - totalused;
                    newuseint = newint + olduseint;
                    //新残日から使用日数の残りを引く。有給残日数を超過してたらreturn
                    if (olduseint < 0)
                    {
                        Debug.WriteLine("newuseint計算確認。newintは？" + newint + "olduseintは？" + olduseint);

                        if (newuseint < 0)
                        {
                            System.Windows.MessageBox.Show("有給残日数が不足しています");
                            dgupdate();
                            //beforeused_rowをリセットする
                            beforeused_row = -1;
                            //beforeusedcheckをリセットする
                            beforeusedcheck = 0;

                            return;
                        }
                    }

                    //使用日数が負　＝　使用日数が減らされた場合の処理
                    if (totalused < 0)
                    {
                        //その年の付与日数を格納
                        int thisyear_add = Convert.ToByte(dr[7]);

                        //旧・新残日用の変数
                        int addnew;
                        int addold;

                        //残日を戻した時、今年付与日数分を超えたら前年欄に入れる処理のための変数
                        if (newint - totalused > thisyear_add)
                        {
                            addnew = thisyear_add;
                            addold = newint - totalused - thisyear_add;

                        }
                        //今年度付与分を超えなかったら新残日からのみ引く
                        else
                        {
                            addnew = newint - totalused;
                            addold = 0;
                        }


                        //使用日数の更新sqlを一旦作成する
                        thissql = "update t_holiday_remaining set " + clist[cnum] + "=\'" + value + "\'" + " where member_id =" + row[0] + " and years=" + thisyear + ";" +

                        //新残日・旧残日を増やしてupdate
                        "update t_holiday_remaining set " + clist[cnum - 1] + "=\'" + addnew + "\' where member_id =" + row[0] + " and years=" + thisyear + ";" +
                        "update t_holiday_remaining set " + clist[cnum - 2] + "=\'" + (oldint + addold) + "\' where member_id =" + row[0] + " and years=" + thisyear + ";";

                        updatesql.Add(thissql);
                        //beforeused_rowをリセットする
                        beforeused_row = -1;
                        //beforeusedcheckをリセットする
                        beforeusedcheck = 0;
                        return;
                    }


                    //使用日数の更新sqlを一旦作成する
                    thissql = "update t_holiday_remaining set " + clist[cnum] + "=\'" + value + "\'" + " where member_id =" + row[0] + " and years=" + thisyear + ";";
                    updatesql.Add(thissql);
                    Debug.WriteLine("usedのsqlは" + thissql);

                    //旧残日から使用日数を引いた値が0か負の場合　＝　旧残日が残って無い場合
                    //旧残日を0にして、新残日から旧残日から使用日を引いた残りの分を引く

                    if (olduseint <= 0)
                    {

                        //旧残日を0にしてupdate                       
                        thissql = "update t_holiday_remaining set " + clist[cnum - 2] + "=\'" + 0 + "\'" + " where member_id =" + row[0] + " and years=" + thisyear + ";" +
                            //新残日から使用日数を引いてupdate
                            "update t_holiday_remaining set " + clist[cnum - 1] + "=\'" + newuseint + "\' where member_id =" + row[0] + " and years=" + thisyear + ";";
                        updatesql.Add(thissql);
                        Debug.WriteLine("旧残日ゼロ：" + thissql);

                    }
                    else
                    {
                        //旧残日が余る場合はその値でupdate
                        thissql = "update t_holiday_remaining set " + clist[cnum - 2] + "=\'" + olduseint + "\'" + " where member_id =" + row[0] + " and years=" + thisyear + ";";
                        updatesql.Add(thissql);
                        Debug.WriteLine("旧残日余り：" + thissql);
                    }

                    Debug.WriteLine("thissqlは" + thissql);
                    //beforeused_rowをリセットする
                    beforeused_row = -1;
                    //beforeusedcheckをリセットする
                    beforeusedcheck = 0;





                    //以下の処理は行わない
                    return;

                }//使用日数計算ここまで
               

                if (cnum < 12)
                {
                    cnum = cnum + (sqlmonth - 4) * 3;

                }

                else if (cnum < 15)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 6;

                }
                else if (cnum < 18)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 12;

                }
                else if (cnum < 21)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 18;
                }
                else if (cnum < 24)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 24;
                }
                else if (cnum < 27)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 30;
                }
                else if (cnum < 30)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 36;
                }
                else if (cnum < 33)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 42;
                }
                else if (cnum < 36)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 48;
                }
                else if (cnum < 39)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 54;
                }
                else if (cnum < 42)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 60;
                }
                else if (cnum < 45)
                {
                    cnum = cnum + (sqlmonth - 4) * 3 - 66;
                }

                try
                {
                    thissql = "update t_holiday_remaining set " + clist[cnum] + "=\'" + value + "\'" + "where member_id =" + row[0] + " and years=" + thisyear + ";";
                }
                catch
                {
                    return;
                }


            }

            Debug.WriteLine("thissql確認：" + thissql+"cnum確認"+cnum);
            updatesql.Add(thissql);

        }//Addsqlここまで

        //新年度の表を作成するメソッド
        private void make_newyear(object sender, RoutedEventArgs e)
        {
            //処理中マウスカーソルを待機マークにする
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

            //年度が空なら処理しない
            if (years_box.Text == "")
            {
                System.Windows.MessageBox.Show("年度が入力されていません");
                return;
            }
            //years_boxが数字以外なら消す。また表作成しない。
            int makeyear = 0;
            try
            {
                makeyear = int.Parse(years_box.Text);
            }
            catch
            {
                years_box.Clear();
                return;
            }

            //２年後以降の表作成は禁止
            if (makeyear > thisyear + 1 || makeyear < 2019)
            {
                System.Windows.MessageBox.Show("来年度以降の表は作成できません。または、誤った年度が入力されています。");
                return;
            }


            //今年度のt_holiday_remainingのデータテーブルを作成する
            DataTable tbl_hr = new DataTable();


            //今年度かつ、新年度表を作成していないテーブルを作成する
            conn.Open();
            using (var command = conn.CreateCommand())
            {
                command.CommandText = $"select * from t_holiday_remaining where names not in (select names from t_holiday_remaining where years = {makeyear}) ;";
                using (var reader = command.ExecuteReader())
                {
                    tbl_hr.Load(reader);
                }
            }
            conn.Close();

            String newyear_sql = "";
            foreach (DataRow r in tbl_hr.Rows)
            {
                //nullセルには0を記入
                for (int i = 3; i < tbl_hr.Columns.Count; i++)
                {
                    if (r[i].ToString() == "")
                    {
                        r[i] = 0;
                    }
                }
                if (r[2].ToString() == thisyear.ToString())
                {
                    newyear_sql =

                        " INSERT INTO `t_holiday_remaining` (`member_id`, `NAMES`, `years`, `april_old`, `april_new`, `april_used`, `may_old`, `may_new`, `may_used`, `june_old`, `june_new`, `june_used`, `july_old`, `july_new`, `july_used`, `august_old`, `august_new`, `august_used`, `september_old`, `september_new`, `september_used`, `october_old`, `october_new`, `october_used`, `november_old`, `november_new`, `november_used`, `december_old`, `december_new`, `december_used`, `january_old`, `january_new`, `january_used`, `february_old`, `february_new`, `february_used`, `march_old`, `march_new`, `march_used`) " +
                         " VALUES (" + r[0] + ",\"" + r[1] + "\"," + makeyear + "," + r[3] + "," + r[4] + "," + r[5] + "," + r[6] + "," + r[7] + "," + r[8] + "," + r[9] + "," + r[10] + "," + r[11] + "," + r[12] + "," + r[13] + "," + r[14] + "," + r[15] + "," + r[16] + "," + r[17] + "," + r[18] + "," + r[19] + "," + r[20] + "," + r[21] + "," + r[22] + "," + r[23] + "," + r[24] + "," + r[25] + "," + r[26] + "," + r[27] + "," + r[28] + "," + r[29] + "," + r[30] + "," + r[31] + "," + r[32] + "," + r[33] + "," + r[34] + "," + r[35] + "," + r[36] + "," + r[37] + "," + r[38] + "); ";

                }

                conn.Open();
                MySqlCommand com2 = new MySqlCommand(newyear_sql, conn);
                try
                {
                    com2.ExecuteReader();
                }
                catch
                {

                }
                conn.Close();
                Debug.WriteLine("SQLチェック" + newyear_sql);

            }

            //新しい年度表の各月日計算
            thisyear = makeyear;
            initialcalc();
            initialsort();
            dgupdate();
            System.Windows.MessageBox.Show(+makeyear + "年度の表を更新しました");

            //thisyearを今年度に戻す
            if (DateTime.Now.Month > 3)
            {
                thisyear = DateTime.Now.Year;
            }
            else
            {
                thisyear = DateTime.Now.Year - 1;
            }
            System.Windows.Forms.Cursor.Current = Cursors.Default;

        }//make_newyearここまで

        //社員情報の表示
        private void showmember(object sender, RoutedEventArgs e)
        {
            if (entry_year.Visibility == Visibility.Hidden)
            {
                entry_year.Visibility = Visibility.Visible;
                working_years.Visibility = Visibility.Visible;
                holiday_division.Visibility = Visibility.Visible;
                edate.Visibility = Visibility.Visible;
            }
            else
            {
                entry_year.Visibility = Visibility.Hidden;
                working_years.Visibility = Visibility.Hidden;
                holiday_division.Visibility = Visibility.Hidden;
                edate.Visibility = Visibility.Hidden;
            }


        }//showmemberここまで


        private void ButtonOnOff(object sender, RoutedEventArgs e)
        {
            if (autocalc)
            {
                autocalc = false;
                System.Windows.MessageBox.Show("自動計算をオフにしました。");
                OnOffButton.Content = "自動計算はオフです";
            }
            else
            {
                autocalc = true;
                System.Windows.MessageBox.Show("自動計算をオンにしました。");
                OnOffButton.Content = "自動計算はオンです";
            }
        }



    }
}

