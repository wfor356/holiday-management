﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MySql.Data.MySqlClient;

using System.Diagnostics;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;

namespace WpfApp1
{
    /// <summary>
    /// Addnewmember.xaml の相互作用ロジック
    /// </summary>
    public partial class Addnewmember : Window
    {
        static string connStr = "server=172.105.241.20; user id=user; password=garhijapan#02; database=holiday_management";
        
        private MySqlConnection conn = new MySqlConnection(connStr);
        public Boolean IsCancel { set; get; }
        int thisyear;
        
        
        public Addnewmember(int a)
        {
            InitializeComponent();
            thisyear = a;
            
            //コンボボックス用のCSVファイル読み込み
            {

                //読み込むCSVファイルを開く
                StreamReader sr = new StreamReader(@"C:\Program Files (x86)\Default Company Name\有給管理システム\事業所名一覧.csv");

                String line = sr.ReadLine();


                while (line != null)
                {

                    txt_memberof.Items.Add(line);
                    line = sr.ReadLine();

                }
            }


        }

        /// <summary>
        /// キャンセルボタンクリックイベント.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.IsCancel = true;
            this.Close();
        }

        /// <summary>
        /// 社員情報新規追加ボタンクリックイベント.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>/// 
        //新規入力した社員データをDBに追加する
        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            this.IsCancel = false;
            addmember();

            
            
        }

        private void addmember()
            {
            int divint = -1;
            try
            {
                DateTime entrydate = DateTime.Parse(txt_entry.Text);
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("入社年月日をyyyy/MM/ddの形式で入力してください");
                return;
            }
            
            try
            {
                divint = int.Parse(txt_division.Text);
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("付与区分に誤りが無いか、または半角数字で入力されているか確認して下さい。");
                return;
            }
            
            if(divint < 0 || divint > 5 || DateTime.Parse(txt_entry.Text) > DateTime.Now)
            {
                System.Windows.MessageBox.Show("・付与区分に誤りが無いか、または半角数字で入力されているか確認して下さい。\n\r・入社日が本日以降になっていないか確認して下さい。");
                return;
            }

            
            
            
            String am = "INSERT into t_member_informations (names,entry_date,member_of,division) values (\'"+ txt_name.Text + "\',\'"  + txt_entry.Text + "\',\'" + txt_memberof.Text + "\',\'" + txt_division.Text + "\');\n\r";
            String hr = "INSERT into t_holiday_remaining (names,years) values (\'" + txt_name.Text + "\',\'" + thisyear + "\');\n\r";
            String idupdate = "UPDATE t_holiday_remaining SET member_id = (SELECT member_id FROM t_member_informations WHERE NAMES =\'"+txt_name.Text +"\') WHERE t_holiday_remaining.names =\'" + txt_name.Text + "\' ;";
            String newmember = am +hr + idupdate;
            Debug.WriteLine(newmember);
            Debug.WriteLine("確認："+idupdate);
            Debug.WriteLine("社員追加SQL"+newmember);

            conn.Open();
            try
            {

                MySqlCommand com = new MySqlCommand(newmember, conn);
                com.ExecuteReader();
                conn.Close();
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("・未入力の項目がないか、または半角数字で入力されているか確認して下さい。");
                conn.Close();
                return;
            }

            System.Windows.MessageBox.Show(txt_name.Text + "さんのデータを登録しました。");

            //登録したらテキストボックスのクリア
            
            txt_name.Clear();
            txt_entry.Clear();
            txt_memberof.SelectedIndex = -1;
            txt_division.SelectedIndex = -1;

            //更新用フラグをtrueにする
            MainWindow.newdata = true;

        }

    }
}